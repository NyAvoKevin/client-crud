import {HTTP} from "../http-config";

class UserDataService{
    
    getAll() {
        return HTTP.get("/user");
    }
    create(data) {
        return HTTP.post("/user", data);
    }
    update(data) {
        return HTTP.put(`/user`, data);
    }
    delete(uuid) {
        return HTTP.delete(`/user/${uuid}`);
    }
}

export default new UserDataService()